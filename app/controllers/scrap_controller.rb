class ScrapController < ApplicationController
  require 'nokogiri'
  require 'open-uri'
  require 'fastimage'
  def url
    
  end
  def load2
    request = params[:q]
    @url = Url.new()
    html = Nokogiri::HTML(open(request))
    
    # Sélection dans la variable @url.title, de la balise h2 si la h1 est absente 
    if html.css('h1') == nil
      title = html.css('h2').text
    else 
      title = html.css('h1').text  
    end
    @url.title = title
    
    # Sélection d'une image valable 
    img_src = []
    img_size = []
    html.css('img').each do |i|
      
      # Trier l'url est ajouter l'adresse du site au répertoire asset et images
      if i['src'].start_with?("/assets") || i['src'].start_with?("/im")
         base_url = request.split("/")[2]
         @u = i['src']
         i['src'] = "http://" + base_url + i['src']
         
      else
        
      end 
      @size_min = FastImage.size(i['src'])
      
      # Sélectionner la 1er image ayant pour largeur un minimum de 300px
      if @size_min.present? && @size_min[0] >= 300
       #img_size = FastImage.size(i['src']) 
       img_src = i['src']
       @url.img_src = img_src
       break
      else 
        img_src = nil
        img_size = nil
      end
    end
    
    
  end

  def load
    url = params[:q]
    
    # Récup du code source 
    html = Nokogiri::HTML(open(url))
    @html = html 
    #title = html.css('h2').first.text
    #@title = title
    # Sélection dans la variable @title, de la balise h2 si la h1 est absente 
    if html.css('h1') == nil
      title = html.css('h2').text
      
    else 
      title = html.css('h1').text
      
    end
    @title = title 
    
    #Extraction des images de la page et sélection d'une image utilisable.
    img_size = []
    img_src = []
    img_srcs = html.css('img').map{ |i| i['src'] } 
    img = img_srcs.each do |src| 
      img_src << src
      img_size << FastImage.size(src)  
    end

    
    @img_size = img_size
    @img_src = img_src
    @img = img
    
  end
  
  
end
