class ScrapController < ApplicationController
  require 'nokogiri'
  require 'open-uri'
  require 'fastimage'
  def url
    
  end
  def load2
    
  end

  def load
    url = params[:q]
    
    # Récup du code source 
    html = Nokogiri::HTML(open(url))
    @html = html 
    #title = html.css('h2').first.text
    #@title = title
    # Sélection dans la variable @title, de la balise h2 si la h1 est absente 
    if html.css('h1') == nil
      title = html.css('h2').text
      
    else 
      title = html.css('h1').text
      
    end
    @title = title 
    
    #Extraction des images de la page et sélection d'une image utilisable.
    img_size = []
    img_src = []
    img_srcs = html.css('img').map{ |i| i['src'] } 
    img = img_srcs.each do |src| 
      img_src << src
      img_size << FastImage.size(src)  
    end

    
    @img_size = img_size
    @img_src = img_src
    @img = img
    
  end
  
end
