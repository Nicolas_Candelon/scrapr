class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string :url
      t.string :title
      t.string :img_src
      t.string :img_size
      t.text :content

      t.timestamps null: false
      
    end
  end
end
